# Documentation template

> *THIS PROJECT HAVE BEEN DEPRECATED IN FAVOUR OF:*
> https://gitlab.com/ElenQ/templates

This is the documentation template for ElenQ Technology.

It uses `MarkDown` as source and converts to PDF output using `pandoc` using
`xelatex` latex engine.

> **WARNING**: It won't work with other latex engine.


# License

This project is distributed under Apache 2.0 license.

Copyright ElenQ Technology.


# Contributors

- Ekaitz Zárraga from ElenQ Technology
- Ondiz Zárraga as external LaTeX consultant


# How to

1. Put the content in `src` folder.
2. Put a metadata block in your content with the author, title, subtitle,
   license and such. Use `article.yaml` and `book.yaml` as reference if you
   need it.
3. Decide if you need a large book-like document or just a small article-like
   document for your content.
4. Use `make book` or `make article` accordingly.
5. Output will be written on `Article.pdf` or `Book.pdf` depending on the step
   4.

Details to consider:

- Makefile gets all the files of the `src` folder with `.md` extension
  ordered alphabetically and concatenates them. If you user more than one
  markdown file, you will need to put numbers as a prefix on the files.
  `0_intro.md`, `1_firstSection.md` and so on.

- The template includes a logo. It is saved in `img` folder. If you want to
  change it, just override the file with the logo you want.

- Metadata files are simple YAML files with some default options. They contain
  a `title`and a `subtitle` just to work as a template. You should change them
  or override those variables on a metadata block in markdown content.
