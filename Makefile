# Mini Makefile for the moment, consider moving to a different tool

C 			= pandoc -N --latex-engine=xelatex

TEMPLATE 	= template.latex

TARGETBOOK	= Book.pdf
TARGETART	= Article.pdf

METABOOK	= book.yaml
METAART		= article.yaml

CHANGELOG	= changes.tex

SRC			= $(sort $(wildcard src/*.md))

book: $(TARGETBOOK)
article: $(TARGETART)

$(TARGETBOOK): $(SRC) $(TEMPLATE) $(METABOOK)
	git for-each-ref --format="%(refname:short) & %(*authordate:short) & %(subject) \\\\" refs/tags > $(CHANGELOG)
	$(C) --template=$(TEMPLATE) $(SRC) $(METABOOK) -o $(TARGETBOOK) --metadata=book:true
$(TARGETART): $(SRC) $(TEMPLATE) $(METAART)
	$(C) --template=$(TEMPLATE) $(SRC) $(METAART) -o $(TARGETART) --metadata=article:true
clean:
	rm -f $(TARGETART) $(TARGETBOOK) $(CHANGELOG)

.PHONY: book article clean
